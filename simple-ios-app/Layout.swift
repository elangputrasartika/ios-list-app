//
//  Layout.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit

enum ThemeLayout:  Int, CaseIterable {
    case button
    
    var insets: UIEdgeInsets {
        switch self {
        case .button:
            return UIEdgeInsets.init(top: 10, left: 16, bottom: 10, right: 16)
        }
    }
}
