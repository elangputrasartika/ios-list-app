//
//  Response.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit

//MARK: Response
struct BasicResponse: Codable {
    let status: Int
    let result: [UserStruct]
}

//MARK: User
struct UserStruct: Codable {
    let id: String
    let nik: String
    let nama: String
    let alamat: String
    let no_telepon: String
    let email: String
    let unit: String
    let status: String
    let role: String
}

