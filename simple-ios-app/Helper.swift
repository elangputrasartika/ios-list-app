//
//  Helper.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit
import Foundation

struct urlTmp: Hashable {
    var url: URL?
    var urlRequest: URLRequest?
}

func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

func asString(jsonDictionary: JSONDictionary) -> String {
  do {
    let data = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
    return String(data: data, encoding: String.Encoding.utf8) ?? ""
  } catch {
    return ""
  }
}

private let domain: String = "https://polar-cove-58817.herokuapp.com"
public let normalSpacing = 16
public let minimumSpacing = 10
public let mediumSpacing = 25

enum Endpoint: String {
    case USERS_LIST = "/api/users"
}

class Helper  {
    var domainGlobal = domain
    
    static func convertToHumanDate(param: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.locale = Locale(identifier: "id")
        let yourDate = formatter.date(from: param)
        formatter.dateFormat = "EEEE, d MMM yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        
        return myStringafd
    }
    
    static func formatPrice(price: Int32) -> String {
        let fmt = NumberFormatter()
        fmt.numberStyle = .decimal
        let res = fmt.string(from: NSNumber(value: price))
        
        return res!
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIView {

  func dropShadow() {
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOffset = CGSize(width: 2, height: 3)
    layer.masksToBounds = false

    layer.shadowOpacity = 0.3
    layer.shadowRadius = 3
    layer.rasterizationScale = UIScreen.main.scale
    layer.shouldRasterize = true
  }

  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}
