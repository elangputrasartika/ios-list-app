//
//  HttpInjection.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit

protocol URLSessionDataTaskProtocol {
    
    func resume()
}

extension URLSessionDataTask: URLSessionDataTaskProtocol { }

protocol URLSessionProtocol: class {
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskProtocol
}

extension URLSession: URLSessionProtocol {

    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskProtocol {
        
        // The double casting is necessary because both methods have the same name and the compiler gets lost
        return ((dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTask) as URLSessionDataTaskProtocol)
    }
}

typealias JSONDictionary = [String: Any]

enum HttpMethod: String {
    
    case GET = "GET"
    case DELETE = "DELETE"
    case POST = "POST"
    case PUT = "PUT"
}

enum HttpError {
    
    case json
    case parsing
    case unknown
}

enum Result<T> {
    
    case failed(HttpError)
    case successful(T)
}

protocol HttpClientProtocol {
    
    func request(endpoint: String, httpMethod: HttpMethod, bodyData: JSONDictionary?, onCompletion: @escaping (Result<JSONDictionary>) -> Void)
}

final class HttpClient: HttpClientProtocol {
    
    private let baseUrl: String
    
    private let urlSession: URLSessionProtocol
    
    convenience init() {
        
        self.init(baseUrl: Helper().domainGlobal, urlSession: URLSession.shared)
    }
    
    init(baseUrl: String, urlSession: URLSessionProtocol) {
        
        self.baseUrl = baseUrl
        self.urlSession = urlSession
    }
    
    func request(endpoint: String, httpMethod: HttpMethod, bodyData: JSONDictionary?, onCompletion: @escaping (Result<JSONDictionary>) -> Void) {
        
        guard let url = URL(string: baseUrl + endpoint) else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        if let bodyData = bodyData, httpMethod == .POST || httpMethod == .PUT {
            
            do {
                
                let json = try JSONSerialization.data(withJSONObject: bodyData, options: [])
                
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                
                request.httpBody = json
            }
            catch {
                
                DispatchQueue.main.async {
                    
                    onCompletion(.failed(.json))
                }
                
                return
            }
        }
        
        let task = urlSession.dataTask(with: request) { (data, urlResponse, error) -> Void in
            
            var errorResponse = HttpError.unknown
            
            do {
                
                if let data = data, data.count > 0,
                    let response = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? JSONDictionary {
                    
                    DispatchQueue.main.async {
                        
                        onCompletion(.successful(response))
                    }
                        
                    return
                }
            }
            catch {
                
                // handle error properly
                errorResponse = .json
            }
            
            DispatchQueue.main.async {
                
                // ideally we'd map the error properly
                onCompletion(.failed(errorResponse))
            }
        }
        
        task.resume()
    }
}

