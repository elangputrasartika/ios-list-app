//
//  User.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit

class User
{
    var id: String = ""
    var nik: String = ""
    var nama: String = ""
    var alamat: String = ""
    var no_telepon: String = ""
    var email: String = ""
    var unit: String = ""
    var status: String = ""
    var role: String = ""
    
    init(id: String, nik: String, nama: String, alamat: String, no_telepon: String, email: String, unit: String, status: String, role: String)
    {
        self.id = id
        self.nik = nik
        self.nama = nama
        self.alamat = alamat
        self.no_telepon = no_telepon
        self.email = email
        self.unit = unit
        self.status = status
        self.role = role
    }
    
    init()
    {
        self.id = ""
        self.nik = ""
        self.nama = ""
        self.alamat = ""
        self.no_telepon = ""
        self.email = ""
        self.unit = ""
        self.status = ""
        self.role = ""
    }
    
    class func fetchUsers(controller: UIViewController, page: Int = 1, _ completion: @escaping ([String: Any]) -> ())
    {
        let httpClient = HttpClient()
        var results: [String: Any] = [:]
        var dict: JSONDictionary = [:]
        httpClient.request(endpoint: Endpoint.USERS_LIST.rawValue + "?page=\(page)", httpMethod: HttpMethod.GET, bodyData: dict, onCompletion: { result in
            switch result {
            case .successful(let str):
                print("asddas")
                print(str)
                print("wewwqe")
                var data: [User]
                data = []
                if (str["status"] as! String) == "success" {
                    print(str)
                    for val in str["data"] as! [JSONDictionary] {
                        print(val)
                        data.append(
                            User(
                                id: val["_id"] as! String,
                                nik: val["nik"] as! String,
                                nama: val["nama"] as! String,
                                alamat: val["alamat"] as! String,
                                no_telepon: val["no_telepon"] as! String,
                                email: val["email"] as! String,
                                unit: val["unit"] as! String,
                                status: val["status"] as! String,
                                role: val["role"] as! String))
                    }
                    results["data"] = data
                    
                    completion(results)
                } else {
                    let alert = UIAlertController(title: "Error", message: (str["message"] as! String), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                    controller.present(alert, animated: true, completion: nil)
                    
                    completion([:])
                }
            case .failed( _):
                let alert = UIAlertController(title: "Error", message: "Something went wrong, please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
                
                completion([:])
            }
        })
    }
}

