//
//  UsersCell.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit
import ShimmerSwift

class UsersCell: UICollectionViewCell {
    var shimmerView: ShimmeringView!
    
    private var lblTeacher: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.text = "nama guru"
        label.textAlignment = .left
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = UIColor.lightGray
        label.backgroundColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var lblClass: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.text = "kelas contoh text"
        label.textAlignment = .left
        label.font = .boldSystemFont(ofSize: 16)
        label.textColor = UIColor.lightGray
        label.backgroundColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    var item: User! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI()
    {
        if item.id != "" {
            shimmerView.isShimmering = false
            
            lblTeacher.text = item.nama
            lblTeacher.backgroundColor = ThemeColors.transparent.rgb
            lblTeacher.textColor = .black
            
            lblClass.text = item.nik
            lblClass.backgroundColor = ThemeColors.transparent.rgb
            lblClass.textColor = .lightGray
        }
    }
    
    func setImage(from url: String, iv: UIImageView) {
        guard let imageURL = URL(string: url) else { return }

        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                iv.image = image
                iv.layer.cornerRadius = 10.0
                iv.layer.masksToBounds = true
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 10
        
        let vw = UIView()
        vw.backgroundColor = .white
        vw.layer.cornerRadius = 10
        vw.translatesAutoresizingMaskIntoConstraints = false
        
        vw.addSubview(lblTeacher)
        lblTeacher.topAnchor.constraint(equalTo: vw.topAnchor, constant: CGFloat(minimumSpacing)).isActive = true
        lblTeacher.leftAnchor.constraint(equalTo: vw.leftAnchor, constant: CGFloat(minimumSpacing)).isActive = true
        lblTeacher.rightAnchor.constraint(equalTo: vw.rightAnchor, constant: CGFloat(-minimumSpacing)).isActive = true
        lblTeacher.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        vw.addSubview(lblClass)
        lblClass.topAnchor.constraint(equalTo: lblTeacher.bottomAnchor, constant: CGFloat(minimumSpacing)).isActive = true
        lblClass.leftAnchor.constraint(equalTo: vw.leftAnchor, constant: CGFloat(minimumSpacing)).isActive = true
        lblClass.rightAnchor.constraint(equalTo: vw.rightAnchor, constant: CGFloat(-minimumSpacing)).isActive = true
        lblClass.bottomAnchor.constraint(equalTo: vw.bottomAnchor, constant: CGFloat(-minimumSpacing)).isActive = true
        lblClass.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shimmerView = ShimmeringView(frame: self.bounds)
        self.addSubview(shimmerView)
        shimmerView.contentView = vw
        vw.topAnchor.constraint(equalTo: topAnchor).isActive = true
        vw.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        vw.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        vw.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        shimmerView.isShimmering = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

