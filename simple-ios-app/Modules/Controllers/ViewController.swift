//
//  ViewController.swift
//  simple-ios-app
//
//  Created by Majesthink on 12/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var UsersCollectionView: UICollectionView!
    var detailController: DetailUserController!
    let reuseId: String = "UsersCell"
    var refreshControl: UIRefreshControl?
    var users: [User] = [User()]
    var page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        User.fetchUsers(controller: self) {
            arr in
            self.users = arr["data"] as! [User]
            DispatchQueue.main.async {
                self.UsersCollectionView.reloadData()
                self.UsersCollectionView.alwaysBounceVertical = true
            }
        }
        
        self.view.backgroundColor = ThemeColors.background.rgb
        
        let flowLayout = UICollectionViewFlowLayout()
        UsersCollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: flowLayout)
        UsersCollectionView.register(UsersCell.self, forCellWithReuseIdentifier: reuseId)
        UsersCollectionView.delegate = self
        UsersCollectionView.dataSource = self
        UsersCollectionView.showsVerticalScrollIndicator = false
        UsersCollectionView.showsHorizontalScrollIndicator = false
        UsersCollectionView.isPagingEnabled = false
        UsersCollectionView.backgroundColor = ThemeColors.background.rgb
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 32
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .vertical
        UsersCollectionView.collectionViewLayout = layout
        UsersCollectionView.isUserInteractionEnabled = true
        
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refreshStream(_:)), for: .valueChanged)

        refreshControl = refresher
        UsersCollectionView.addSubview(refreshControl!)
        UsersCollectionView.translatesAutoresizingMaskIntoConstraints = false

        self.view.addSubview(UsersCollectionView)
        UsersCollectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        UsersCollectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        UsersCollectionView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor).isActive = true
        UsersCollectionView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Daftar Pengguna"
        
        
        let bottomRefreshController = UIRefreshControl()
        bottomRefreshController.triggerVerticalOffset = 50
        bottomRefreshController.addTarget(self, action: #selector(refreshBottom(_:)), for: .valueChanged)

        UsersCollectionView.bottomRefreshControl = bottomRefreshController
    }
    
    @objc func refreshStream(_ sender: UIRefreshControl) {
        User.fetchUsers(controller: self) {
            arr in
            self.users = arr["data"] as! [User]
            DispatchQueue.main.async {
                self.UsersCollectionView.reloadData()
                self.UsersCollectionView.alwaysBounceVertical = true
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    @objc func refreshBottom(_ sender: UIRefreshControl) {
        page += 1
        User.fetchUsers(controller: self, page: page) {
            arr in
            if ((arr["data"] as! NSArray).count > 0) {
                self.users.append(contentsOf: arr["data"] as! [User])
                DispatchQueue.main.async {
                    self.UsersCollectionView.reloadData()
                    self.UsersCollectionView.bottomRefreshControl?.endRefreshing()
                }
            } else {
                let alert = UIAlertController(title: "Last Page", message: "You have been reached the last page", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                DispatchQueue.main.async {
                    self.UsersCollectionView.bottomRefreshControl?.endRefreshing()
                }
            }
         }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = users[indexPath.item]
        
        detailController = (self.storyboard?.instantiateViewController(withIdentifier: "DetailPage") as! DetailUserController)
        detailController.user = item
        self.navigationController?.pushViewController(detailController!, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! UsersCell
        cell.backgroundColor = UIColor.white
        cell.item = users[indexPath.item]
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 2
        cell.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: (collectionView.bounds.size.width - 32), height: 70)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 16, bottom: 5, right: 16)
    }
}
