//
//  DetailUserController.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit

class DetailUserController: UIViewController {
    var user: User!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNik: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = ThemeColors.background.rgb
        
        lblName.text = user.nama
        lblNik.text = user.nik
        lblAddress.text = user.alamat
        lblPhone.text = user.no_telepon
        lblEmail.text = user.email
        lblUnit.text = user.unit
        lblStatus.text = user.status
        lblRole.text = user.role
    }
}
