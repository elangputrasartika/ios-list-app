//
//  Colors.swift
//  simple-ios-app
//
//  Created by Majesthink on 13/11/20.
//  Copyright © 2020 Majesthink. All rights reserved.
//

import UIKit

enum ThemeColors:  Int, CaseIterable {
    case background
    case tintColor
    case cardHome
    case buttonDarkBlue
    case ellipse
    case transparent
    case subLabel
    case muted
    case borderField
    case placeholder
    case btnLabel
    
    var rgb: UIColor {
        switch self {
        case .background:
            return UIColor(red: 249/255, green: 249/255, blue: 251/255, alpha: 1)
        case .tintColor:
            return UIColor(red: 0/255, green: 14/255, blue: 75/255, alpha: 1)
        case .cardHome:
            return UIColor(red: 103/255, green: 176/255, blue: 243/255, alpha: 1)
        case .ellipse:
            return UIColor(red: 65/255, green: 156/255, blue: 223/255, alpha: 1)
        case .buttonDarkBlue:
            return UIColor(red: 83/255, green: 83/255, blue: 203/255, alpha: 1)
        case .subLabel:
            return UIColor(red: 156/255, green: 169/255, blue: 181/255, alpha: 1)
        case .muted:
            return UIColor(red: 183/255, green: 183/255, blue: 184/255, alpha: 1)
        case .borderField:
            return UIColor(red: 197/255, green: 118/255, blue: 207/255, alpha: 1)
        case .placeholder:
            return UIColor(red: 226/255, green: 226/255, blue: 226/255, alpha: 1)
        case .btnLabel:
            return UIColor(red: 65/255, green: 156/255, blue: 223/255, alpha: 1)
        case .transparent:
            return UIColor.init(white: 1, alpha: 0)
        }
    }
}
